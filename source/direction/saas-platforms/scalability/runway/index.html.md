---
layout: markdown_page
title: "Product Direction - Runway"
description: "Runway is GitLab's internal PaaS, focused on providing paved roads for stage teams."
---

- TOC
{:toc}

## Context & Overview

The current application architecture runs most GitLab application features inside of our software monolith. However, many new features require different resources and tools, implemented in different languages, with large libraries, and have different hardware requirements. Adding these features to the existing infrastructure will increase the size of the GitLab application container rapidly, resulting in slower startup times, increased number of dependencies, security risks, negatively impacting development velocity, and increasing complexity.

Runway is an emerging internal platform as a service (PaaS) that will allow GitLab stage teams to rapidly deploy and iterate on new features implemented as sattelite services. The initial technical blueprint which inspired this platform is the [GitLab Service-Integration: AI and Beyond](https://docs.gitlab.com/ee/architecture/blueprints/gitlab_ml_experiments/) blueprint and we have since written a [Runway Blueprint](https://docs.gitlab.com/ee/architecture/blueprints/runway/).

The [AI-Gateway](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/) is the first service deployed using Runway. This has validated runway as a model and we're working to expand the feature-set and make Runway self-serve to grow adoption.

## Goals

1. Create "paved roads" for stage teams looking to deliver features to customers and simplify deploying new GitLab features & services.
1. Enable stage teams to engage with a "You build it, you run it" philosophy and empower them to move safer, sooner and happier.
1. Materially reduce MTTP as well as toil associated with a production release (readiness reviews, security reviews etc.).
1. Collaborate with other stakeholders on a "well architected framework" to give "approved by default" architectures for new services.
1. Grow and expand Runway to become the default pattern to deploy new SaaS services.
1. Lower the cost of experimentation & solution validation for all teams

## Challenges
<!-- Optional section. What are our constraints? (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

GitLab Stage teams within the [Development Department](https://about.gitlab.com/handbook/engineering/development/) do not typically have SRE capability. Teams are not usually set up or staffed with SREs or 'SRE like' expectations including things like on-call and operational support. From a responsibility, expectations and Jobs To Be Done point of view the SRE Capability and operational responsibility of the software lifecycle is pushed to other groups like the [Delivery](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/), [Reliability](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/) and [Scalability](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/) groups. One of Runway's explicit goals is to empower teams to be able to run the services that they build and benefit from an end to end DevSecOps workflow. Over time, this will require that stage teams gain some SRE capability. However, the extent of the SRE capability that is pragmatic for the business along with the blend of SRE capability that Runway will provide is yet to be decided.

Additionally, Runway is a new capability and not an extension of existing capability. So far it has relied on minimal staffing and our [borrow](https://about.gitlab.com/handbook/product/product-processes/#borrow) process to bring it to life. In order to bring runway to the maturity we desire and increase the adoption to a level where it becomes the default, we need to have a structured support model and clear expectations that customers can rely on. This should come in the form of staffing a team dedicated to Runway over time along with on-call support.

## 1-Year Plan

<!-- Describe key themes, projects, and/or features planned over the next year. Also highlight what we will not be doing in the next year -->
Over the next 12 months, we plan to work on the following areas to meet our goals. You can track the progress in our [Runway Roadmap Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/969)

<!-- May need updating as more AI initiatives come in -->

### Adopt Internal Product Naming Conventions

GitLab maintains [definitions for different product phases](https://docs.gitlab.com/ee/policy/experiment-beta-support.html) so that customers and team members can use a common language and maintain understanding of support and product expectations. We have chosen to [adopt these definitions](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/98#decision-2023-10-04), with some caveats and additions. The naming conventions are written from the frame of reference of customer facing features and not backend/infrastructure services and therefore are missing some key assumptions of reliability, resilience and quality that we hold as minimum standards for infrastructure services.

We are starting this by defining our criteria and the work required for a [Beta release](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1115) of the Runway platform. Our main focus will be covering a core feature-set of capability required for most services and increasing the number of services deployed via Runway.


After the Beta release, we will focus on a [GA release](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1123), exanding the number of platform features supported and growing the number of services deployed via Runway. 

### Increase Adoption & Mature the Platform

Over time we want Runway to become the default deployment option for satellite services at GitLab. We want to create paved roads that allow teams to spin up production services that are secure, reliable and scalable in minutes not days/weeks. Additionally, we'll work with other groups/stakeholders like [Product Sec](https://handbook.gitlab.com/handbook/security/security-engineering/product-security-engineering/) and Reliability to create "Well Architected" defaults that enable teams to bypass heavyweight production readiness and scalability assessments with "pre-approved" architectures.

### A note on 'keeping the lights on'

Not known at present is what the long term operational responsibility matrix will look like for services deployed Via Runway. We have an end goal of enabling teams to build and run their own services, but this may not be feasible until the platform is more mature. Instead of building heavyweight process in from the start, we are championing GitLab's [Iteration value](https://handbook.gitlab.com/handbook/values/#iteration) and going to learn by doing, making changes as we need to. Longer term this will likely evolve into a well structured and defined process that requires changes in the organisation to escape Conway's law.

### What we're not doing

We’re not working on deploying the monolith or any of our core services. We are not focused on how to include Runway deployed services into the GitLab package.

### Services Currently Deployed using Runway

This will likely move away from this page to an inventory over time.

1. [AI-Gateway](https://docs.gitlab.com/ee/architecture/blueprints/ai_gateway/)

The list above can change and should not be taken as a hard commitment.
For the most up-to-date information about our work, please see our [Runway roadmap](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/969).
